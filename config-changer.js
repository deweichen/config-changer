'use strict';

// Assuming you're running this script at jacks.infrastructure/docker/env/configs/
// To change schema version from 17 to 18:
// echo "./\ndevelopment.json\nschemaVersion\n17\n18" | node config.changer

const fs       = require("fs");
const readline = require("readline");

function changeValue(filePath, keyName, oldVal, newVal, done) {
  const tmpFile = filePath + ".tmp";
  const writer = fs.createWriteStream(tmpFile, 'utf8');
  const lineReader = readline.createInterface({
    input: fs.createReadStream(filePath, 'utf8')
  });
  let changed = false;
  lineReader.on('line', (line) => {
    if (line.indexOf(keyName) !== -1 && line.indexOf(oldVal) !== -1) {
      writer.write(line.replace(oldVal, newVal) + "\n");
      changed = true;
    } else {
      writer.write(line + "\n");
    }
  });
  lineReader.on('close', () => {
    writer.end();
    if (changed) {
      fs.renameSync(tmpFile, filePath);
      done.changedFiles.push(filePath);
    } else {
      fs.unlinkSync(tmpFile);
    }
    done.num++;
  });
}

// BFS traversal starting at rootPath looking for targetFileName and runs action on that file
function walkDir(rootPath, targetFileName, actionFunc, callback) {
  const matchedFiles = [];
  const queue        = [];  // FIFO queue of directory paths
  queue.push(rootPath);
  while (queue.length > 0) {
    let curPath = queue.shift();
    let fileNames = fs.readdirSync(curPath);
    for (let i = 0; i < fileNames.length; i++) {
      let filePath = curPath + '/' + fileNames[i];
      let stat = fs.lstatSync(filePath);
      if (stat.isDirectory()) {
        queue.push(filePath);
      } else if (stat.isFile() && fileNames[i] === targetFileName) {
        actionFunc(filePath);
        matchedFiles.push(filePath);
      }
    }
  }
  return callback(null, matchedFiles);
}

function main() {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
  });
  rl.question("Directory (do not include trailing /): ", (ans) => {
    const rootPath = ans;
    rl.question(`File to change under ${rootPath}: `, (ans) => {
      const fileName = ans;
      // check if json file
      if (fileName.indexOf(".json") === -1) {
        console.error("Error: File must contain '.json'");
        return process.exit(-1);
      }
      rl.question(`Key to change in ${fileName}: `, (ans) => {
        const keyName = ans;
        rl.question(`Old value for ${keyName}: `, (ans) => {
          const oldVal = ans;
          rl.question(`New value for ${keyName}: `, (ans) => {
            const newVal = ans;
            const done = { num: 0, changedFiles: [] };
            const actionFunc = (filePath, callback) => {
              changeValue(filePath, keyName, oldVal, newVal, done);
            };
            console.log("Working...");
            walkDir(rootPath, fileName, actionFunc, (err, matchedFiles) => {
              if (err) {
                console.error(err);
                return process.exit(-2);
              }
              // wait for files to be complete
              const interval = setInterval(() => {
                if (done.num === matchedFiles.length) {
                  if (done.changedFiles.length > 0) {
                    console.log("Following files have been updated:");
                    for (var i = 0; i < done.changedFiles.length; i++) {
                      console.log(done.changedFiles[i]);
                    }
                  } else {
                    console.log("No changes made");
                  }
                  clearInterval(interval)
                  process.exit(0);
                }
              }, 1000);
            });
          });
        });
      });
    });
  });
}

main();